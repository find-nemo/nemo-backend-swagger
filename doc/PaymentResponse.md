# openapi.model.PaymentResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**uId** | **String** |  | 
**driverId** | **int** |  | 
**childId** | **int** |  | 
**amount** | **String** |  | 
**hasDriverAccepted** | **bool** |  | 
**hasParentAccepted** | **bool** |  | 
**dates** | **String** |  | 
**status** | **String** |  | 
**creatorId** | **int** |  | 
**createdAt** | **String** |  | 
**notes** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


